---
title: "Diferential Evolution"
slug: "diferential-evolution-implementation"
date: 2018-06-26
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---


[Codigo de python](https://gitlab.com/senjuana/machine_learning/snippets/1728386)



### > TLDR

- Implementanción de Diferential Evolution.
- Documentacion acerca del algoritmo [aqui](http://www1.icsi.berkeley.edu/~storn/code.html).

### > Parametros

Las Dependencias para inplementar el algoritmo son las siguientes:

```python
import numpy as np
import random as rand
import matplotlib.pyplot as plt
import math
```
Hacemos una funcion que contendra todo desde los parametos hasta el algoritmo para poder corer todo en un main de python.
Despues  hacemos la inicializacion de parametros importantes tales como el numero de individuos, las dimensiones de la funcion objetivo,
la probabilidad de mutacion y el operador de cruzamiento.

```python
def main():
    Np = 40 #Numero de individuos
    D = 2
    Cr = 0.9 #Probabilidad de ser mutado de un individuo
    F = 0.5  #Operador de cruzamiento

    VectorV = np.empty((Np, D))
    VectorU = np.empty((Np, D))

    #Inicializar los arreglos
    for i in range(Np):
        for j in range(D):
            VectorV[i][j] = rand.randint(-20, 20)
```
Se puede cambiar los valores de estas variables para poder observar el diferente funcionamiento del algoritmo.

Iniciamos la grafica que estara ploteando en tiempo real la evolucion de nuestros induviduos.
```python
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)
```


### > Algoritmo

Iniciamos el contador para solo tener 200 iteraciones y creamos en el rango de la poblacion los tres valores de r para poder tomar valores aleatorios
en la siguiente generacion y crearlas mutaciones de forma aleatoria.

```python
    NumEvaluaciones = 0
    while(NumEvaluaciones < 200):
        for i in range(Np):
            r0 = i
            while(r0 == i):
                r0 = math.floor(rand.random() * Np)
            r1 = r0
            while(r1 == r0 or r1 == i):
                r1 = math.floor(rand.random() * Np)
                r2 = r1
            while(r2 == r1 or r2 == r0 or r2 == i):
                r2 = math.floor(rand.random() * Np)
                
            jrand = math.floor(rand.random() * D)
```

Una vez que se tienen los indices para las corespondiente mutaciones de la siguiente poblacion se revisa la probabilidad de mutación 
para generar o no mutaciones con su operador de cruzamiento.

```python
            for j in range(D):
                if (rand.random() <= Cr or j == jrand):
                    #Mutación
                    VectorU[i][j] = VectorV[r0][j] + F * (VectorV[r1][j] - VectorV[r2][j])
                else:
                    VectorU[i][j] = VectorV[i][j]
```

Despues procedemos a evaluar los valores en la funcion objetivo o fitness function.
```python
        for k in range(Np):
            if fitness(VectorU[k][0], VectorU[k][1]) < fitness(VectorV[k][0], VectorV[k][1]):
                VectorV[k][0] = VectorU[k][0]
                VectorV[k][1] = VectorU[k][1]
```
En este paso ya tenemos la poblacion actual y la poblacion futura y las ploteamos en con diferente signo en la grafica.

```python
        line1 = ax.plot(VectorU[:, 0], VectorU[:, 1],'b+')
        line2 = ax.plot(VectorV[:, 0], VectorV[:, 1],'g*')

        ax.set_xlim(-10, 20)
        ax.set_ylim(-10, 20)
        
        fig.canvas.draw()

        ax.clear()
        ax.grid(True)
        
        NumEvaluaciones += 1

        print ('Número de evaluación: ' + str(NumEvaluaciones))
```
### > Resultados

Una vez que terminamos las iteraciones del algoritmo mostramos el vectorV que contiene en cada casilla las cordenadas en la funcion objetivo
donde convergieron los individuos atravez de las docientas iteraciones.

```python 
    print ('VectorV: ')
    print (VectorV)

def fitness(x, y):
    #Funcion Rosenbrock en 2D
    return 100 * ((y - (x**2))**2) + ((1 - (x**2))**2)

if __name__ == "__main__":
    main()
```

En este caso la funcion objetivo o  fitness es la funcion de rosenbrock, pero puede ser cambiada por cualquien funcion objetivo que se desee.

           



