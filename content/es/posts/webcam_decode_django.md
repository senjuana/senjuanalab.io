---
title: "Decodificado de imagenes django"
slug: "decoding-imagenes-django"
date: 2018-06-07
draft: false
tags: ["Desarrollo Web", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---


[Codigo de python](https://gitlab.com/Technovasoft/dif-albergue/snippets/1722444)


En un proyecto nos vimos en la necesidad de ofrecer la solución mas facil y rapida de implementar para 
pasar imagenes tomadas por html5 del cual se pueden ver ejemplos [aqui](https://www.html5rocks.com/es/tutorials/getusermedia/intro/) y la solucion mas rapida para pasar las imagenes fue pasarlas como una string base 64 por medio de un input.

### > TLDR

- `Codificación` la codificacion base64 es un estandar de manejo de imagenes
- Se necesita hacer la implementacion mas corta y rapida posible
- Usar Ajax y otras tecnologias no se pensaba por reto. ¯\\\_(ツ)_/¯

### > Extension

Esto fue una de las partes menos problematicas.

```python
def get_file_extension(file_name, decoded_file):
	import imghdr

	extension = imghdr.what(file_name, decoded_file)
	extension = "jpg" if extension == "jpeg" else extension

	return extension
```

Una vez la imagen ya estuviera decodificada es trivial darle una extension.

### > Split

Se comprueba que sea un string correcta en base64 para poder decodificar.
```python
# Checa si es un string base64
if isinstance(data, six.string_types):
	# Checa si la string bse64 esta en el "data:" formato
	if 'data:' in data and ';base64,' in data:
		# Rompe la string para quitar el header de la información
		header, data = data.split(';base64,')
```
Despues intentamos decodificar la información ya spliteada.

```python 
	try:
		decoded_file = base64.b64decode(data)
	except TypeError:
		TypeError('invalid_image')
```
### > Imagen

Una vez que se decodifica la imagen necesitamos generar el archivo.

```python
	# Genera el nombre del archivo:
	file_name = str(uuid.uuid4())[:12] # 12 caracteres para el nombre .

	# Obtiene la extencion:
	file_extension = get_file_extension(file_name, decoded_file)

	complete_file_name = "%s.%s" % (file_name, file_extension, )

	return ContentFile(decoded_file, name=complete_file_name)
```

### > Utilizacion

La utilizacion es un facil como esto, se puede usar en campos ImageFiled y FileField.

```python
	base = request.POST['foto']

	#modelo hipotetico
	ingre = Internos(telefono=telefono,foto=decode_base64_file(base))
    ingre.save()
```


