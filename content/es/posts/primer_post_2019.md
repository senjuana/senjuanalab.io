---
title: "Primer Post del año"
slug: "primer post del año"
date: 2019-03-17
draft: false
tags: ["personal","tl;dr"]
categories: ["Opinion"]
authors:
- "Ernesto Ruiz"
---

## > 2018 TL;DR 
* Termine un verano cientifico.
* Termine un sistema de manejo de albergues.
* Consegui un nuevo trabajo como web developer jr.

## > 2019 
Este post era un post para resumir las cosas mas importantes que me pasaron 
y dar una perspectiva para este año 2019 pero pues ya pasaron casi 3 meses
de que empezo el año, asi que mas bien pondre una lista de cosas que quiero 
hacer con este blog y en general.


**Blog**

* Decidir si este sera el tema definitivo (Den).
* Publicar post tecnicos mas explicados que no parescan notas tecnicas.
* Publicar el post del proyecto de mi verano cientifico.

**Opinion**

* Empezar a publicar post de opinion.
* Postear a modo de diario mis esfuerzos de liberarme de muchas de las google apps.
* Hacer un esfuerzo de mantener las publicaciones mas frecuentes (ya falle un poco por minimo 3 meses).


