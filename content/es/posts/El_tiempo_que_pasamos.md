---
title: "Te amo"
slug: "te_amo"
date: 2020-07-05
draft: false
tags: ["Opinion"]
categories: ["Opinion"]
authors:
- "Ernesto Ruiz"
---

# Introducción 
Veremos como crear una Api Restful usando Flask y SQLite. 
[Codigo](https://gitlab.com/senjuana/py-robot)

# Requisitos 
- Python = 3.x.x
- requirements.txt [aqui](https://gitlab.com/senjuana/py-robot/blob/master/requirements.txt)

# Instalar requerimientos 
Todos los requerimientos que no sean python escribelos en un txt llamado requirements.txt

    $ pip install -r requirements.txt 

Esto instalara nuestros requerimientos de python. 

# Base de datos 
Este ejemplo usa Sqlite que es una base de datos que se maneja en un archivo, el cual puedes bajar desde [aqui](https://gitlab.com/senjuana/py-robot/blob/master/mydatabase.db).
Puedes tambien hacer tus conectores a las bases de datos que necesites. 

# App basica 
Crearemos un solo archivo para manejar los cuatro metodos Listar,Subir,Actualizar,Borrar.

Empezamos con las librerias que usaremos
```python
#!/usr/bin/python3
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
```

Ahora pondremos la conexion a la base de datos (el archivo tiene que estar en la misma carpeta que este archivo), y las varible de inicio
de el servidor de flask. 

```python
db_connect = create_engine('sqlite:///mydatabase.db')
app = Flask(__name__)
api = Api(app)
```

Hacemos una funcion para observar si funciona nuestro server.
```python
@app.route("/")
def hello():
    return "Hello World"
```

Al final del archivo tenemos el main que correra nuestra aplicacion en la ip publica de nuestra pc y en el puerto 8080.
```python
if __name__ == '__main__':
     app.run(host='0.0.0.0', port=8080)
```

Corremos el server como cualquier archivo de python

    $ python server.py

Para confirmar que nuestra función de hola mundo funciona entramos a localhost:8080
![Image](https://i.imgur.com/Qf1y1hy.png)

# List and Create

Ahora crearemos  una clase para la creacion de nuevos robots y listar los que ya existen en la base de datos. 

Creamos la clase robot que tendra los dos metodos get y post, en el metodo get obtendremos la lista de robots que esta guaradados
en la base de datos.
Hacemos la conexion a la base de datos, y despues ejecutamos el query que nos devolvera todos los registros de 
nuestra tabla robot. 

Una vez que tenemmos query usamos esta funcion de python para poder recorrer este resultado registro por registro, romperlo 
en tuplas de datos y usar zip para guardar las tublas en un diccionario.

```python
class Robot(Resource):
    def get(self):
        conn = db_connect.connect() # conecta a la base de datos
        query = conn.execute("select * from robot") # Esta linea regresa la list ade objetos que se regresaran en un json 
        result = [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]
        return jsonify(result)
```

Ahora que tenemos los registros de la base de datos separados en una lista de diccionarios solo nos queda regresarlo con la funcion
de jsonify que hace que esta lista de diccionarios se regresen en formato json.


```python
class Robot(Resource):
    ...
    def post(self):
        conn = db_connect.connect()
        ids = request.json['id']
        method = request.json['method']
        name = request.json['name']
        query = conn.execute("insert into robot values('{0}','{1}','{2}')".format(ids,name,method))
        return {'status':'success'}
```





