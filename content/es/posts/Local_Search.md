---
title: "Local Search"
slug: "local-search-implementation"
date: 2018-06-25
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---


[Codigo de matlab](https://gitlab.com/senjuana/machine_learning/snippets/1728131)



### > TLDR

- Implementanción de clim hill.
- Funcion [forrester](https://gitlab.com/senjuana/machine_learning/snippets/1728138) que se usa en este ejemplo.

### > Parametros

Para poder implementar esta version de local search nmo ocumpamas mucho mas que la funcion objetivo 
en la cual se buscara el local minimo o maximo.

```matlab
%%Inicio de parametros
data = [];
for i=1:50
    data(i) = forrester(i); 
end

figure(1)
plot(data)
grid on

```
De esta forma creamos la funcion objetivo y la ploteamos.

### > Algoritmo

Lo primero que se hace es iniciar un punto aleatorio en la funcion que sera tomado como en local maximo 
o minimo segun que se busque, en este caso es el maximo local.

```matlab

%% algoritmo
Best =  randi([1 50],1,1); %mejor inicial 
disp(['Mejor Inicial: ' num2str(Best)]);

token = 1; % token de estancamiento
Aux = 0;
iter = 1;

```
En el while se tiene un token para evitar el estancamiento del algoritmo, se hacen varias validaciones 
para que no se salga del search space y solo observen.

```matlab

while  token ~= 3

    iter = iter + 1;
    % creacion de los vecinos
    R = Best-1; 
    L = Best+1;
 
    % validacion de search space
    if R < 1
       token = 3; 
    end
    if L > 50
       token = 3; 
    end
    %update de el mejor
    if forrester(Best) < forrester(R)
        Best = R;
    end
    if forrester(Best) < forrester(L)
        Best = L;
    end
    %validacion del estancamiento
    if Best == Aux
       token = token + 1; 
    end
     Aux = Best;
end

```

### > Resultados

Una vez que se termina el el algoritmo es seguro que alcanzo el minimo o maximo local y procedemos a imprimirlo.

```matlab

%% Resultado
disp(['terminado en ' num2str(iter) ' iteraciones.']);
disp(['Cordenadas del mejor local: ' num2str(Best) ' y: ' num2str(forrester(Best))]);

```






