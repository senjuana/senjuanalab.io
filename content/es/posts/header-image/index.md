---
title: "Shortcodes"
date: 2018-03-05T16:01:23+08:00
lastmod: 2018-03-05T16:01:23+08:00
draft: true
tags: ["images"]
categories: ["Opinion"]
authors:
- "senjuana"
resources:
- name: header 
  src: 'header.png'
---

# Shortcodes
## Twiiter

{{< tweet 877500564405444608 >}}

## mastodon
{{% mastodon status="https://mastodon.social/@kevingimbel/100700713283716694" %}}
## mastodon
{{% mastodon status="https://mastodon.social/@kevingimbel/100700713283716694" width="600" height="300" %}}

## Google Map


{{< googlemaps id="17_6iCOL6LkRjIFGPKmXBxjsvbBc" height="400">}}


## Figure

{{<figure src="/images/globe.svg" alt="Globe" align="aligncenter" width="300" caption="**Globe**">}}


## Highlight for Shells

{{< shhighlight bash "hl_lines=2 4" >}}
# test
echo test
# just a test
echo hello world
{{< /shhighlight >}}

## gist

{{< gist spf13 7896402 >}}


<script src="https://gitlab.com/senjuana/machine_learning/snippets/1728131.js"></script>
