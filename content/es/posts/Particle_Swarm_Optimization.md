---
title: "Particle Swarm Optimization"
slug: "pso-implementation"
date: 2018-07-27
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---

[Codigo de matlab](https://gitlab.com/senjuana/machine_learning/snippets/1737820)

### > TLDR

- Implementanción de Particle Swarm Optimization
- Documentacion acerca del algoritmo [aqui](http://www.cs.us.es/~fsancho/?e=70).
- El mejor tutorial que encontre acerca del tema [aqui](http://yarpiz.com/440/ytea101-particle-swarm-optimization-pso-in-matlab-video-tutorial).

### > Parametros

Primero se inician variables importantes como las dimensiones del espacio de busqueda y el numero de particulas
en la poblacion, junto con los valores de coeficiente que son usados para modificar las velocidades y las direcciones
de las particulas en el proceso del algoritmo.

```matlab
d = 2; %limites del espacio de busqueda
l = [-5, -5]; %limite inferior
u = [10, 10]; %limite superior
Max_iter = 150; %Maximo numero te iteraciones de PSO
Part_N = 50; %Cantidad de particulas en la poblacion

% valores de coeficientes
w = 1;  % coeficiente inercial 
wdamp = 0.99; % Damping ratio of Inertia coefficent  
c1 = 0; % coeficiente personal de aceleracion
c2 = 2; % coeficiente social de aceleracion

%Proceso de inicializacion de PSO 
x = l(1) + rand(Part_N,d).* (u(1) - l(1));

%Evalua la funcion objetivo
for i = 1:Part_N
	obj_func(i,:) = rosenbrock(x(i,:));
end
```

Una vez que se tiene las particulas inicializadas junto con las variables importantes se pasa buscar valores 
optimos segun lo que se busca en la poblacion inicial de particulas y iniciar las velocidades en zeros.

```matlab
%Obtiene el mejor valor global (minimo)
[glob_opt, ind] = min(obj_func);
%Vector de optimo global
%Vector de unos del tamaño de la poblacion
G_opt = ones(Part_N,d);
%Valores del optimo en dimension 1
G_opt(:,1) = x(ind,1);
%Valores del optimo en dimension 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%Mejor local para cada particula
Loc_opt = x;
%Inicializa velocidades
v = zeros(Part_N,d);
%
```

### > Algoritmo

Una vez que la poblacion inicial es evaluada pasamos a un ciclo que estara corriendo hasta que las iteraciones 
que nosotros propusimos sean cumplidas y se actualizan las velocidades de las particulas, despues se tiene que 
evaluar particula por particula en el espacio de busqueda y se analiza cual es la que esta mas cerca del optimo 
buscado.

```matlab
while t < Max_iter

%Calcula la nueva velocidad ecuacion 4.2
v = w * v + c1 * rand(Part_N,d) .* (Loc_opt - x) + c2 * rand(Part_N,d) .* (G_opt - x);
%Calcula nueva posicion ecuacion 4.3
x = x + v;

for i = 1:Part_N
    %Se evaloan las nuevas posiciones en la funcion objetivo
    Nva_obj_func(i,:) = rosenbrock(x(i,:));
    %Se verifica si se actualizan los optimos locales
    if Nva_obj_func(i,:) < obj_func(i,:)
        %Actualiza optimo local
        Loc_opt(i,:) = x(i,:);
        %Actualiza funcion objetivo
        obj_func(i,:) = Nva_obj_func(i,:);
    end
end
```

Despues de que cada particula fue evaluada en el espacio de busqueda se analiza su proximidad a el optimo 
buscado y se actualizan las variables de evolucion y la inercia que llevan las particulas que seran sumadas
a sus velocidades en el proximo ciclo.

```matlab
%Obtiene el mejor valor global (minimo)
[Nvo_glob_opt, ind] = min(obj_func);
%Se verifica si se actualiza el optimo global
if Nvo_glob_opt < glob_opt
    glob_opt = Nvo_glob_opt;
    %Valores del optimo en dimension 1
    G_opt(:,1) = x(ind,1);
    %Valores del optimo en dimension 2
    G_opt(:,2) = x(ind,2);
    Mejor_pos = [x(ind,1),x(ind,2)];
end
%Almacena los valores de funcion objetivo en cada iteracion
Evol_func_obj(t) = glob_opt;
%Incrementa iteraciones
t = t + 1;

%Damping inertia Coefficient
w = w * wdamp;
end

```
Una vez que termina el ciclo por que alcanzo las iteraciones propuestas ploteamos la evolucion del optimo 
buscado sobre el tiempo y la mejor posicion obtenida junto con el optimo global del espacio de busqueda.

```matlab
figure(1);
plot(Evol_func_obj)
disp(['Mejor posicion x: ',num2str(Mejor_pos)])
disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])
```

## > Resultados

Primero tendras una grafica que tendra una forma parecida a esta, que es la evolucion del optimo sobre el tiempo
que va obteniendo el algoritmo.

![Image](https://i.imgur.com/XbsiMU2.png)

Despues en la consola tendras los resultados numericos de la mejor posicion y el optimo.

![Image](https://i.imgur.com/8GtAQn9.png)

