---
title: "Harmony Search"
slug: "harmony-search-implementation"
date: 2018-11-10
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---


[Codigo de matlab](https://gitlab.com/senjuana/machine_learning/snippets/1777729)



### > TLDR

- Implementanción del Algoritmo de Harmony Search.
- Documentacion acerca del algoritmo [aqui](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4405318/).
- Función Objetivo Rosenbrock.

### > Parametros

- Primero nos aseguramos que no existan variables de otras corridas en la memoria de matlab.
- Inicializamos las variables que definiran el espacio de busqueda y las dimensiones de la función 
objetivo.
- Inicializamos las varibles de armonias que seran usadas en el algoritmo.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777732.js"></script>

- Iniciamos la variable que funcionara como memoria o respaldo de las armonias que 
funcionan mejor.
- Evaluamos la funcion en funcion del numero de armonias para generar un espacio de busqueda.
- Evaluamos en el espacio de busqueda las armonias iniciales y buscamos en este caso la menor.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777733.js"></script>

### > Algoritmo

- Se revisa primero	las armonias en función del numero de particulas utilizadas.
- Se evalua la nueva armoniaen las función objetivo y se compara con el peor valor y el mejor.
- se saca los mejor valores de esta corrida y se guarda el menoy o el mayor segun lo buscado.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777735.js"></script>

### > Resultados
Se grafica la evolución de la mejor armonia atravez del tiempo que estuvo funcionando el algoritmo.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777736.js"></script>

En este caso la funcion objetivo o  fitness es la funcion de rosenbrock, pero puede ser cambiada por cualquien funcion objetivo que se desee.

           



