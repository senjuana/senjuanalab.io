---
title: "Harmony Search"
slug: "harmony-search-implementation"
date: 2018-11-10
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Tecnicos"]
authors:
- "Ernesto Ruiz"
---


[Codigo de matlab](https://gitlab.com/senjuana/machine_learning/snippets/1777729)



### > TLDR

- Implementation of  Harmony Search.
- Documentation about the  algorithm [here](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4405318/).
- Objective Function Rosenbrock.

### > Parameters

- First we make sure that there are no variables from other runs in matlab's memory.
- Initialize the variables that define the search space and the dimensions of the function Objective.
- We initialize the harmony varibles that will be used in the algorithm.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777732.js"></script>

- We started the variable that worked as memory or support of the harmonies that 
work better.
- We evaluate the function according to the number of harmonies to generate a search space.
- We evaluate the initial harmonies in the search space and look for the smallest in this case.

<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777733.js"></script>

### > Algorithm
- The harmonies are first revised according to the number of particles used.
- The new harmony is evaluated in the objective function and compared with the worst value and the best.
- The best values are taken from this run and the menoy or the greater is kept according to what is searched for.
<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777735.js"></script>

### > Results
It graphs the evolution of the best harmony through the time that the algorithm was working.
<script src="https://gitlab.com/senjuana/machine_learning/snippets/1777736.js"></script>

In this case the objective function or fitness is the rosenbrock function, but it can be changed by whoever objective function is desired.

           



