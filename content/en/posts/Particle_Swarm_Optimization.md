---
title: "Particle Swarm Optimization"
slug: "pso-implementation"
date: 2018-07-27
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Technical"]
authors:
- "Ernesto Ruiz"
---


[Matlab Code](https://gitlab.com/senjuana/machine_learning/snippets/1737820)



### > TLDR

- Implementation of Particle Swarm Optimization
- Documentation about the algorithm [here](http://www.cs.us.es/~fsancho/?e=70).
- The best tutorial I found about the subject [here](http://yarpiz.com/440/ytea101-particle-swarm-optimization-pso-in-matlab-video-tutorial).

### > Parameters

First, important variables such as the dimensions of the search space and the number of particles are initiated.
In the population, along with the coefficient values that are used to modify the velocities and directions
of the particles in the algorithm process.

```matlab
d = 2; %limites del espacio de busqueda
l = [-5, -5]; %limite inferior
u = [10, 10]; %limite superior
Max_iter = 150; %Maximo numero te iteraciones de PSO
Part_N = 50; %Cantidad de particulas en la poblacion

% valores de coeficientes
w = 1;  % coeficiente inercial 
wdamp = 0.99; % Damping ratio of Inertia coefficent  
c1 = 0; % coeficiente personal de aceleracion
c2 = 2; % coeficiente social de aceleracion

%Proceso de inicializacion de PSO 
x = l(1) + rand(Part_N,d).* (u(1) - l(1));

%Evalua la funcion objetivo
for i = 1:Part_N
	obj_func(i,:) = rosenbrock(x(i,:));
end
```

Once you have the initialised particles together with the important variables, you go on to look for values. 
We optimize according to what is searched in the initial population of particles and to initiate the speeds in zeros.

```matlab
%Obtiene el mejor valor global (minimo)
[glob_opt, ind] = min(obj_func);
%Vector de optimo global
%Vector de unos del tamaño de la poblacion
G_opt = ones(Part_N,d);
%Valores del optimo en dimension 1
G_opt(:,1) = x(ind,1);
%Valores del optimo en dimension 2
G_opt(:,2) = x(ind,2);
Mejor_pos = [x(ind,1),x(ind,2)];
%Mejor local para cada particula
Loc_opt = x;
%Inicializa velocidades
v = zeros(Part_N,d);
%
```


### > Algorithm


Once the initial population is evaluated we move on to a cycle that will be running until the iterations 
that we proposed are fulfilled and the velocities of the particles are updated, then it has to be done. 
Evaluate particulate by particulate in the search space and analyze which is closer to the optimum. 
Wanted.

```matlab
while t < Max_iter

%Calcula la nueva velocidad ecuacion 4.2
v = w * v + c1 * rand(Part_N,d) .* (Loc_opt - x) + c2 * rand(Part_N,d) .* (G_opt - x);
%Calcula nueva posicion ecuacion 4.3
x = x + v;

for i = 1:Part_N
    %Se evaloan las nuevas posiciones en la funcion objetivo
    Nva_obj_func(i,:) = rosenbrock(x(i,:));
    %Se verifica si se actualizan los optimos locales
    if Nva_obj_func(i,:) < obj_func(i,:)
        %Actualiza optimo local
        Loc_opt(i,:) = x(i,:);
        %Actualiza funcion objetivo
        obj_func(i,:) = Nva_obj_func(i,:);
    end
end
```
After of each particle it's evaluated in the searh space, it analize the proximmity to the optime you're
searching and update the variables of evolution and the inertia, these vars need to be added to the velocity 
var of the particles.

```matlab
%Obtiene el mejor valor global (minimo)
[Nvo_glob_opt, ind] = min(obj_func);
%Se verifica si se actualiza el optimo global
if Nvo_glob_opt < glob_opt
    glob_opt = Nvo_glob_opt;
    %Valores del optimo en dimension 1
    G_opt(:,1) = x(ind,1);
    %Valores del optimo en dimension 2
    G_opt(:,2) = x(ind,2);
    Mejor_pos = [x(ind,1),x(ind,2)];
end
%Almacena los valores de funcion objetivo en cada iteracion
Evol_func_obj(t) = glob_opt;
%Incrementa iteraciones
t = t + 1;

%Damping inertia Coefficient
w = w * wdamp;
end

```
One that the cycle ends, it ends because the algorithm reach the number of iterations.
```matlab
figure(1);
plot(Evol_func_obj)
disp(['Mejor posicion x: ',num2str(Mejor_pos)])
disp(['Mejor valor funcion objetivo: ',num2str(glob_opt)])
```


## > Results

First you would  have a graph with the evolution in time of the best value of the algorithm.


![Image](https://i.imgur.com/XbsiMU2.png)


After in the console you would have the results of the best position and best optime value.

![Image](https://i.imgur.com/8GtAQn9.png)




           



