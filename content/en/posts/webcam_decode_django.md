---
title: "Decoding Images in Django"
slug: "decoding-imagenes-django"
date: 2018-06-07
draft: false
tags: ["Desarrollo Web", "notas", "tutorial"]
categories: ["Technical"]
authors:
- "Ernesto Ruiz"
---


[Python code](https://gitlab.com/Technovasoft/dif-albergue/snippets/1722444)

In a project we face the need to implement a quick and easy way to use [this](https://www.html5rocks.com/es/tutorials/getusermedia/intro/)
and upload it to the backend in python, and we use a base 64 string.

### > TLDR

- `Coding` the code base64 it's a standard to manage the images in a light way.
- We need to make a quick way to use that utility.
- Use Ajax and other technologies was out of the question, and we think use base 64 as challange.¯\\\_(ツ)_/¯

### > Extension

This was one of the less problematic parts.

```python
def get_file_extension(file_name, decoded_file):
	import imghdr

	extension = imghdr.what(file_name, decoded_file)
	extension = "jpg" if extension == "jpeg" else extension

	return extension
```

Once the image it’s decode its’s trivial give them an extension.

### > Split
We check that the string it's in base64.
```python
# Checa si es un string base64
if isinstance(data, six.string_types):
	# Checa si la string bse64 esta en el "data:" formato
	if 'data:' in data and ';base64,' in data:
		# Rompe la string para quitar el header de la información
		header, data = data.split(';base64,')
```

Later we try decode the information that it’s already split.

```python 
	try:
		decoded_file = base64.b64decode(data)
	except TypeError:
		TypeError('invalid_image')
```
### > Image

Once that it's decoded, we need to make the file.

```python
	# Genera el nombre del archivo:
	file_name = str(uuid.uuid4())[:12] # 12 caracteres para el nombre.

	# Obtiene la extencion:
	file_extension = get_file_extension(file_name, decoded_file)

	complete_file_name = "%s.%s" % (file_name, file_extension, )

	return ContentFile(decoded_file, name=complete_file_name)
```

### > Utilization
The use it's as easy as this, we can use it in fields ImageField and FileField.

```python
	base = request.POST['foto']

	#modelo hipotetico
	ingre = Internos(telefono=telefono,foto=decode_base64_file(base))
    ingre.save()
```


