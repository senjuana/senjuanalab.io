---
title: "Diferential Evolution"
slug: "diferential-evolution-implementation"
date: 2018-06-26
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Technical"]
authors:
- "Ernesto Ruiz"
---


[Codigo de python](https://gitlab.com/senjuana/machine_learning/snippets/1728386)



### > TLDR

- Implementantion of Diferential Evolution.
- Documentation about the algorithm [here](http://www1.icsi.berkeley.edu/~storn/code.html).

### > Parameters

The Dependencies to implement the algorithm are the following:

```python
import numpy as np
import random as rand
import matplotlib.pyplot as plt
import math
```
We make a function that will contain everything from the parametos to the algorithm to be able to run everything in a python main.
Then we do the initialization of important parameters such as the number of individuals, the dimensions of the objective function,
the mutation probability and the crossing operator.

```python
def main():
    Np = 40 #Numero de individuos
    D = 2
    Cr = 0.9 #Probabilidad de ser mutado de un individuo
    F = 0.5  #Operador de cruzamiento

    VectorV = np.empty((Np, D))
    VectorU = np.empty((Np, D))

    #Inicializar los arreglos
    for i in range(Np):
        for j in range(D):
            VectorV[i][j] = rand.randint(-20, 20)
```
The values of these variables can be changed in order to observe the different functioning of the algorithm.

We initiate the graph that will be plotting in real time the evolution of our individuals.
```python
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)
```


### > Algorithm

We start the counter to only have 200 iterations and create in the population range the three r values to be able to take random values.
In the next generation and create mutations randomly.

```python
    NumEvaluaciones = 0
    while(NumEvaluaciones < 200):
        for i in range(Np):
            r0 = i
            while(r0 == i):
                r0 = math.floor(rand.random() * Np)
            r1 = r0
            while(r1 == r0 or r1 == i):
                r1 = math.floor(rand.random() * Np)
                r2 = r1
            while(r2 == r1 or r2 == r0 or r2 == i):
                r2 = math.floor(rand.random() * Np)
                
            jrand = math.floor(rand.random() * D)
```

Once you have the indices for the corresponding mutations of the following population you check the probability of mutation 
to generate or not mutations with your crossover operator.

```python
            for j in range(D):
                if (rand.random() <= Cr or j == jrand):
                    #Mutación
                    VectorU[i][j] = VectorV[r0][j] + F * (VectorV[r1][j] - VectorV[r2][j])
                else:
                    VectorU[i][j] = VectorV[i][j]
```
Then we proceed to evaluate the values in the target function or fitness function.
```python
        for k in range(Np):
            if fitness(VectorU[k][0], VectorU[k][1]) < fitness(VectorV[k][0], VectorV[k][1]):
                VectorV[k][0] = VectorU[k][0]
                VectorV[k][1] = VectorU[k][1]
```
In this step we already have the current population and the future population and we plot them in different sign in the graph.

```python
        line1 = ax.plot(VectorU[:, 0], VectorU[:, 1],'b+')
        line2 = ax.plot(VectorV[:, 0], VectorV[:, 1],'g*')

        ax.set_xlim(-10, 20)
        ax.set_ylim(-10, 20)
        
        fig.canvas.draw()

        ax.clear()
        ax.grid(True)
        
        NumEvaluaciones += 1

        print ('Número de evaluación: ' + str(NumEvaluaciones))
```
### > Results

Once we finish the iterations of the algorithm we show the vectorV that contains in each box the coordinates in the objective function
where individuals converged through the 200 iterations.

```python 
    print ('VectorV: ')
    print (VectorV)

def fitness(x, y):
    #Funcion Rosenbrock en 2D
    return 100 * ((y - (x**2))**2) + ((1 - (x**2))**2)

if __name__ == "__main__":
    main()
```
In this case the objective function or fitness is the rosenbrock function, but it can be changed by whoever objective function is desired.
           



