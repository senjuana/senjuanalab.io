---
title: "Local Search"
slug: "local-search-implementation"
date: 2018-06-25
draft: false
tags: ["Computo evolutivo", "notas", "tutorial"]
categories: ["Technical"]
authors:
- "Ernesto Ruiz"
---


[Codigo de matlab](https://gitlab.com/senjuana/machine_learning/snippets/1728131)



### > TLDR

- Implementation of clim hill.
- Function [forrester](https://gitlab.com/senjuana/machine_learning/snippets/1728138) that it's used in this program.

### > Parameters

To implement this version of local search you don't need much more than the objective function 
in which the minimum or maximum location will be searched.

```matlab
%%Inicio de parametros
data = [];
for i=1:50
    data(i) = forrester(i); 
end

figure(1)
plot(data)
grid on

```
In this way we create the objective function and plot it.

### > Algorithm

The first thing to do is to initiate a random point in the function that will be taken as the maximum local. 
Or minimum depending on what you are looking for, in this case is the local maximum.

```matlab

%% algoritmo
Best =  randi([1 50],1,1); %mejor inicial 
disp(['Mejor Inicial: ' num2str(Best)]);

token = 1; % token de estancamiento
Aux = 0;
iter = 1;

```
In while there is a token to avoid the stagnation of the algorithm, several validations are made, so he doesn't leave the search space and just watch.
```matlab

while  token ~= 3

    iter = iter + 1;
    % creacion de los vecinos
    R = Best-1; 
    L = Best+1;
 
    % validacion de search space
    if R < 1
       token = 3; 
    end
    if L > 50
       token = 3; 
    end
    %update de el mejor
    if forrester(Best) < forrester(R)
        Best = R;
    end
    if forrester(Best) < forrester(L)
        Best = L;
    end
    %validacion del estancamiento
    if Best == Aux
       token = token + 1; 
    end
     Aux = Best;
end

```

### > Results
Once the algorithm is finished it is sure that it reaches the local minimum or maximum, and we proceed to print it.
```matlab

%% Resultado
disp(['terminado en ' num2str(iter) ' iteraciones.']);
disp(['Cordenadas del mejor local: ' num2str(Best) ' y: ' num2str(forrester(Best))]);

```






