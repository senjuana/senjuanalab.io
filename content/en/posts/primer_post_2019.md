---
title: "First post of the year"
slug: "First post of the year"
date: 2019-03-17
draft: false
tags: ["personal","tl;dr"]
categories: ["Opinion"]
authors:
- "Ernesto Ruiz"
---

## > 2018 TL;DR 
* Finish a scientific summer.
* Complete a shelter management system.
* I got a new job as web developer jr.

## > 2019 
This post was a post to summarize the most important things that happened to me
and give a perspective for this year 2019 but it has been almost 3 months
I started the year, so I'll put a list of things that I want
do with this blog and in general.

**Blog**

* Decide if this will be the final issue (Den).
* Publish more explained technical posts that do not match technical notes.
* Publish the post of the project of my scientific summer.

**Opinion**

* Start posting opinion post.
* Post to journal mode my efforts to free myself from many of the google apps.
* Make an effort to keep the most frequent publications (and fail a little for at least 3 months).

