---
title: "About"
date: 2017-08-20T21:38:52+08:00
lastmod: 2017-08-28T21:41:52+08:00
menu: "main"
weight: 50
---


# José Ernesto Ruiz Valdivia
Student of Software engineering in [Tecnologico MM](http://www.tecmm.edu.mx/)

## Skills
- Web Developer 
- Python Developer
- Spring Developer
- PHP Developer


### CONTACT

* Email: jose.ernesto@protonmail.com
* Telegram: @senjuana
* Matrix: @senjuanaa:matrix.org

