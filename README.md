[![Build status](https://gitlab.com/senjuana/senjuana.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/senjuana/senjuana.gitlab.io/commits/master)
---
Pagina usando Gitlab Pages.

Aprende mas acerca de GitLab Pages en https://pages.gitlab.io y su documentación oficial https://docs.gitlab.com/ce/user/project/pages/.

---

**Tabla de Contenidos**  

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

## GitLab CI

Las paginas estaticas de este proyecto estan contruidas con [GitLab CI][ci], siguiendos los pasos
de [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

Para trabajar localmente este proyecto, tienes que seguir los siguientes pasos:

1. Fork, clona o Descarga este projecto
1. [Instala][] Hugo
1. Corre el Poryecto: `hugo server`
1. Agrega tu contenido
1. Genera el Website: `hugo` (opcional)

Lee mas de la documentación de Hugo [documentation][].

### Preview your site

Si clonas o descargas este proyecto correlo con  `hugo server`,
Tu sitio puede seer accesado en la direccion`localhost:1313/`.

Los temas que son usados estan adaptados de:
- http://themes.gohugo.io/beautifulhugo/
- https://themes.gohugo.io/hugo-coder/
- https://themes.gohugo.io/hugo-theme-den/
- https://themes.gohugo.io/lanyon/

## GitLab User or Group Pages
Para usar este proyecto como  Website de usuario/grupo, necesitas hacer un paso adicional
Paso: solo renombra tu proyecto como `namespace.gitlab.io`, donde `namespace` es
tu nombre de usuario onombre de grupo. Esto se puede hacer llendo a los **Settings** de tu proyecto.

Necesitaras configurar tu sitio tambien: cambia este linea
en tu `config.toml`, de `"https://pages.gitlab.io/hugo/"` a `baseurl = "https://namespace.gitlab.io"`.
Procede igual si estas usando un [custom domain][post]: `baseurl = "http(s)://example.com"`.

Lee mas en  [user/group Pages][userpages] y [project Pages][projpages].

## Did you fork this project?

Si le hiciste Fork a este proyecto para tu propio uso, porfavor ve a las **Settings**
de tu proyecto y elimina la relacion de fork, la cual no es necesaria 
al menos que quieras contribuir al proyecto original.

## Troubleshooting

1. CSS is missing! Esto significa dos cosas:

    Ya sea que configuraste mal el link CSS URL en tu template, o 
	tu pagina estatica tiene una opcion de configuración que se necesita explicitamente
	habilitar para servir los assets estaticos.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[instala]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
